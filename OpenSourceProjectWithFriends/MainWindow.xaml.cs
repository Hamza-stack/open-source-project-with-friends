﻿using System;
using System.Windows;

namespace OpenSourceProjectWithFriends
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MathsOperationsButtonClick(object sender, RoutedEventArgs e)
        {
            MathsOperationsForm form = new MathsOperationsForm();
            form.Show();
            form.Focus();
        }

        private void FibonnaciButtonClick(object sender, RoutedEventArgs e)
        {
            FibonnaciForm form = new FibonnaciForm();
            form.ShowDialog();
            form.Focus();
        }

        private void FizzBuzzButton_Click(object sender, RoutedEventArgs e)
        {
            FizzBuzzForm form = new FizzBuzzForm();
            form.ShowDialog();
            form.Focus();
        }
    }
}
