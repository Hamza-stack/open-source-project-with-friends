﻿using System;
using System.Windows;

namespace OpenSourceProjectWithFriends
{
    /// <summary>
    /// Interaction logic for MathsOperationsForm.xaml
    /// </summary>
    public partial class MathsOperationsForm : Window
    {
        public MathsOperationsForm()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            int number1 = int.Parse(Number1TextBox.Text);
            int number2 = int.Parse(Number2TextBox.Text);

            int result = MathsOperations.CalculateNumbersAdded(number1, number2);
            ResultTextBox.Text = result.ToString();
        }

        private void SubtractButton_Click(object sender, RoutedEventArgs e)
        {
            int number1 = int.Parse(Number1TextBox.Text);
            int number2 = int.Parse(Number2TextBox.Text);

            int result = MathsOperations.CalculateNumbersSubtracted(number1, number2);
            ResultTextBox.Text = result.ToString();
        }

        private void MultiplyButton_Click(object sender, RoutedEventArgs e)
        {
            int number1 = int.Parse(Number1TextBox.Text);
            int number2 = int.Parse(Number2TextBox.Text);

            int result = MathsOperations.CalculateNumbersMultiplied(number1, number2);
            ResultTextBox.Text = result.ToString();
        }

        private void DivideButton_Click(object sender, RoutedEventArgs e)
        {
            int number1 = int.Parse(Number1TextBox.Text);
            int number2 = int.Parse(Number2TextBox.Text);

            int result = MathsOperations.CalculateNumbersDivided(number1, number2);
            ResultTextBox.Text = result.ToString();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Number1TextBox.Text = "";
            Number2TextBox.Text = "";
        }
    }
}
