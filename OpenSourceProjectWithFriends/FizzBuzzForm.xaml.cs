﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OpenSourceProjectWithFriends
{
    /// <summary>
    /// Interaction logic for FizzBuzzForm.xaml
    /// </summary>
    public partial class FizzBuzzForm : Window
    {
        private StringBuilder stringBuilder;

        public FizzBuzzForm()
        {
            InitializeComponent();

            stringBuilder = new StringBuilder();
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            int maxNumber = int.Parse(NumbersTextBox.Text);

            stringBuilder.Clear();

            for (int number = 1; number < maxNumber + 1; number++)
            {
                string fizzBuzzValue = FizzBuzz.GetFizzBuzz(number);
                stringBuilder.Append(number + $": {fizzBuzzValue}" + "\n");
            }

            FizzBuzzNumbersTextBlock.Text = stringBuilder.ToString();
        }
    }
}
