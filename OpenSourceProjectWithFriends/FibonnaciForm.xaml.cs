﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Text;

namespace OpenSourceProjectWithFriends
{
    /// <summary>
    /// Interaction logic for FibonnaciForm.xaml
    /// </summary>
    public partial class FibonnaciForm : Window
    {
        public FibonnaciForm()
        {
            InitializeComponent();
        }

        private void CalculateFibonnaciButton_Click(object sender, RoutedEventArgs e)
        {
            int maxNumbers = int.Parse(NumberTextBox.Text);

            List<int> fibonnaciNumbers = Fibonnaci.Calculate(maxNumbers);

            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < fibonnaciNumbers.Count - 2; i++)
            {
                stringBuilder.Append(fibonnaciNumbers[i].ToString() + ", ");
            }

            ValuesTextBox.Text = stringBuilder.ToString();
        }
    }
}
