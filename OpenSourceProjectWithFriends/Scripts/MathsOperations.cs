﻿using System;

namespace OpenSourceProjectWithFriends
{
    public class MathsOperations
    {
        public static int CalculateNumbersAdded(int x, int y)
        {
            return x + y;
        }

        public static int CalculateNumbersSubtracted(int x, int y)
        {
            return x - y;
        }

        public static int CalculateNumbersDivided(int x, int y)
        {
            if (y == 0)
            {
                return 0;
            }
            return x / y;
        }

        public static int CalculateNumbersMultiplied(int x, int y)
        {
            return x * y;
        }
    }
}
