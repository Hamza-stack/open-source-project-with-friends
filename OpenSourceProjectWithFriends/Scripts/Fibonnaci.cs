﻿using System;
using System.Collections.Generic;

namespace OpenSourceProjectWithFriends
{
    public class Fibonnaci
    {
        public static List<int> Calculate(int numbersToCalculateUpto)
        {
            List<int> calculatedNumbers = new List<int>();

            int num1 = 0, num2 = 1, latestValue = 0;

            calculatedNumbers.Add(num1);
            calculatedNumbers.Add(num2);

            for (int i = 2; i < numbersToCalculateUpto; i++)
            {
                latestValue = num1 + num2;
                num1 = num2;
                num2 = latestValue;
                calculatedNumbers.Add(latestValue);
            }

            return calculatedNumbers;
        }
    }
}
