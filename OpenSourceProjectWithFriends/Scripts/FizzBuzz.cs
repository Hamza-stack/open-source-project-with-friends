﻿using System;

namespace OpenSourceProjectWithFriends
{
    public class FizzBuzz
    {
        public static string GetFizzBuzz(int number)
        {
            if (IsMultipleOf(number, 3) && IsMultipleOf(number, 5))
            {
                return "FizzBuzz";
            }
            else if (IsMultipleOf(number, 3))
            {
                return "Fizz";
            }
            else if (IsMultipleOf(number, 5))
            {
                return "Buzz";
            }
            else
            {
                return "";
            }
        }

        private static bool IsMultipleOf(int number, int divisible) => number % divisible == 0;
    }
}
