using NUnit.Framework;
using OpenSourceProjectWithFriends;

namespace Testing
{
    [TestFixture]
    public class MathsTest
    {
        [Test]
        [TestCase(5, 5, 10)]
        [TestCase(1, 7, 8)]
        public void Test_That_Two_Numbers_Are_Added(int number1, int number2, int expected)
        {
            int actual = MathsOperations.CalculateNumbersAdded(number1, number2);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(10, 5, 5)]
        [TestCase(100, 59, 41)]
        public void Test_That_Two_Numbers_Are_Subtracted(int number1, int number2, int expected)
        {
            int actual = MathsOperations.CalculateNumbersSubtracted(number1, number2);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(4, 2, 2)]
        [TestCase(90, 45, 2)]
        [TestCase(1, 0, 0)]
        public void Test_That_Two_Numbers_Are_Divided(int number1, int number2, int expected)
        {
            int actual = MathsOperations.CalculateNumbersDivided(number1, number2);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(100, 100, 10000)]
        [TestCase(6, 4, 24)]
        public void Test_That_Two_Numbers_Are_Multiplied(int number1, int number2, int expected)
        {
            int actual = MathsOperations.CalculateNumbersMultiplied(number1, number2);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}