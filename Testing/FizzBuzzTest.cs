﻿using NUnit.Framework;
using OpenSourceProjectWithFriends;

namespace Testing
{
    [TestFixture]
    public class FizzBuzzTest
    {
        [Test]
        [TestCase(3, "Fizz")]
        [TestCase(6, "Fizz")]
        [TestCase(9, "Fizz")]
        public void Test_That_Number_Is_Fizz(int number, string expected)
        {
            string actual = FizzBuzz.GetFizzBuzz(number);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(5, "Buzz")]
        [TestCase(10, "Buzz")]
        [TestCase(20, "Buzz")]
        public void Test_That_Number_Is_Buzz(int number, string expected)
        {
            string actual = FizzBuzz.GetFizzBuzz(number);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(15, "FizzBuzz")]
        [TestCase(30, "FizzBuzz")]
        [TestCase(45, "FizzBuzz")]
        public void Test_That_Number_Is_FizzBuzz(int number, string expected)
        {
            string actual = FizzBuzz.GetFizzBuzz(number);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
